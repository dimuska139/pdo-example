<?php

/**
 * Created by PhpStorm.
 * User: dimuska139
 * Date: 15.10.16
 * Time: 14:23
 */
class Connection
{
    private $pdo;
    private $tablename;

    function Connection($table = 'users') {
        $this->tablename = $table;
        $dsn = 'mysql:host='.Settings::HOST.';dbname='.Settings::DBNAME.';charset='.Settings::CHARSET;
        $opt = array(
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        );
        try {
            $this->pdo = new PDO($dsn, Settings::USER, Settings::PASSWORD, $opt);
        } catch (PDOException $e) {
            die('Подключение не удалось. Ошибка: ' . $e->getMessage());
        }
    }

    /**
     * Returns single record by id
     * @param $id
     * @return mixed
     */
    public function getRecordById($id)
    {
        $query = $this->pdo->prepare('SELECT id, name, status FROM '.$this->tablename.' WHERE id=?');
        $query->execute([$id]);
        return $query->fetch();
    }

    /**
     * Changes status from 0 to 1 or from 1 to 0
     * @param $id
     */

    public function changeStatus($id)
    {
        $query = $this->pdo->prepare('UPDATE '.$this->tablename.' SET status = CASE WHEN status=1 THEN 0 ELSE 1 END 
        WHERE id=?');
        $query->execute([$id]);
    }

    public function getRandomRecordId()
    {
        $ids = [];
        $query = $this->pdo->query('SELECT id FROM '.$this->tablename);
        while ($row = $query->fetch())
        {
            $ids[] = $row['id'];
        }
        return $ids[mt_rand(0, count($ids)-1)];
    }

    /**
     * Writes records to base
     * @param $data
     */
    public function insert($data)
    {
        /*$query = $this->pdo->prepare("INSERT INTO ".$this->tablename." VALUES (NULL, ?, ?)");
        foreach ($data as $ind => $item) {
            $query->execute([$item[0], $item[1]]);
        }*/

        $insArr = []; // Dataset to insert
        foreach ($data as $line) {
            foreach ($line as $column) {
                $insArr[] = $column;
            }
        }

        if (!empty($insArr)) {
            $valuesTemplate = array_fill(0, count($insArr)/2, '(NULL, ?, ?)'); // Preparing template
            $valuesTemplate = implode(',', $valuesTemplate);
            $query = $this->pdo->prepare("INSERT INTO ".$this->tablename." VALUES ".$valuesTemplate);
            foreach ($insArr as $ind => $item) {
                if ($ind%2==0) {
                    $query->bindValue($ind + 1, $item, PDO::PARAM_STR); // name
                } else
                    $query->bindValue($ind + 1, $item, PDO::PARAM_INT); // status
            }
            $query->execute();
        }
    }

    /**
     * Uploads data from csv-file
     * @param $csv
     */
    public function loadData($csv)
    {
        $firstLine = true;
        if (($handle = fopen($csv, "r")) !== FALSE) {
            $fromCsv = [];
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                if ($firstLine) // First line ignoring
                {
                    $firstLine = false;
                    continue;
                }
                $fromCsv[] = [$data[0], $data[1]];
            }
            $this->insert($fromCsv);
            fclose($handle);
        } else {
            die('Ошибка при открытии CSV-файла');
        }
    }

    /**
     * Checks if table exists
     * @return bool
     */
    public function existsTable()
    {
        $result = $this->pdo->query('SHOW TABLES LIKE "'.$this->tablename.'"');
        return empty($result->rowCount())?false:true;
    }

    /**
     * Creates new table
     */
    public function createTable()
    {
        $columns = 'id INT(11) AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(200) NOT NULL,
            status TINYINT(1) NOT NULL';
        try {
            $this->pdo->exec('CREATE TABLE IF NOT EXISTS '.$this->tablename.'('.$columns.')');
        } catch (PDOException $e) {
            die('Ошибка при создании таблицы: ' . $e->getMessage());
        }
    }

    /**
     * Returns name of table
     * @return string
     */
    public function getTablename()
    {
        return $this->tablename;
    }
}