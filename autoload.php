<?php
/**
 * Created by PhpStorm.
 * User: dimuska139
 * Date: 15.10.16
 * Time: 14:27
 */

spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});