<?php
/**
 * Created by PhpStorm.
 * User: dimuska139
 * Date: 15.10.16
 * Time: 13:08
 */
require 'autoload.php';



$connection = new Connection();
if (!$connection->existsTable())
{
    $connection->createTable();
    $connection->loadData(Settings::CSVFILE);
}
$randomId = $connection->getRandomRecordId();
$connection->changeStatus($randomId);
$record = $connection->getRecordById($randomId);
$record['status'] = $record['status']==1?0:1;
echo $record['name'].';'.$record['status'];